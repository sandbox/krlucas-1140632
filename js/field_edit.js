Drupal.behaviors.field_edit = function() {
  /**
   * Submit event handler for inline forms.
   */
  $('#field-edit-form').submit(function() {
    if ($(this).find('input[name="apply_to_all"]:checked').length) {
      var nids = '';
      $('.vbo-select:checked').each(function(){
        nids = nids + $(this).closest('tr').find('.chk-select-row').text() +',';
      });
      $(this).find('input[name="nids"]').val($(this).find('input[name="nids"]').val() + ',' + nids);
    }
  });
}